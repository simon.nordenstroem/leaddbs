d = {
    'voxel_arr_MRI': 0,
    'voxel_arr_DTI': 0,
    'Init_neuron_model_ready': 0,
    'Init_mesh_ready': 0,
    'Adjusted_neuron_model_ready': 0,
    'CSF_mesh_ready': 0,
    'Adapted_mesh_ready': 0,
    'signal_generation_ready': 0,
    'Parallel_comp_ready': 0,
    'Parallel_comp_interrupted': 0,
    'IFFT_ready': 0,
    'MRI_data_name': 'segmask.nii',
    'MRI_in_m': 0,
    'DTI_data_name': '',
    'DTI_in_m': 0,
    'CSF_index': 3.0,
    'WM_index': 2.0,
    'GM_index': 1.0,
    'default_material': 3,
    'Electrode_type': 'Boston_Scientific_Vercise_Cartesia',
    'Brain_shape_name': '0',
    'Aprox_geometry_center': [10.0379, -18.2943, -6.5889],
    'Approximating_Dimensions': [100.0, 100.0, 100.0],
    'Implantation_coordinate_X': 11.7816,
    'Implantation_coordinate_Y': -13.5587,
    'Implantation_coordinate_Z': -8.5489,
    'Second_coordinate_X': 13.8119,
    'Second_coordinate_Y': -10.2629,
    'Second_coordinate_Z': -3.3634,
    'Rotation_Z': 20.88,
    'encap_thickness': 0.1,
    'encap_tissue_type': 2,
    'encap_scaling_cond': 1.0,
    'encap_scaling_perm': 1.0,
    'pattern_model_name': '0',
    'Axon_Model_Type': 'McIntyre2002',
    'diam_fib': [5.7],
    'n_Ranvier': [28],
    'v_init': -80.0,
    'Neuron_model_array_prepared': 1,
    'Name_prepared_neuron_array': 'Allocated_axons.h5',
    'EQS_core': 'QS',
    'Skip_mesh_refinement': 1,
    'el_order': 2,
    'number_of_processors': 1,
    'FEniCS_MPI': 0,
    'current_control': 1,
    'Phi_vector': [-0.001, None, None, None, None, None, None, None],
    'Solver_Type': 'GMRES',
    'freq': 130.0,
    'T': 60.0,
    't_step': 1.0,
    'phi': 0.0,
    'Signal_type': 'Rectangle',
    'Ampl_scale': 1.0,
    'CPE_activ': 0,
    'Full_Field_IFFT': 0,
    'spectrum_trunc_method': 'Octave Band Method',
    'trunc_param': 1000.0,
    'Truncate_the_obtained_full_solution': 0,
    'external_grounding': 1,
    'Stim_side': 0,
    'stretch': 1.078499806572898,
    }
